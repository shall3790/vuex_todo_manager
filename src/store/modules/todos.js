import axios from 'axios';

const state = {
    todos: []
};

const getters = {
  allTodos: state => state.todos
};

const actions = {
    async fetchTodos({ commit }) {
        const response = await axios.get(
          'https://jsonplaceholder.typicode.com/todos'
        );
        // console.log(response.todos)
        commit('setTodos', response.data);
      },
    async addTodo({ commit }, title) {
        const response = await axios.post(
            'https://jsonplaceholder.typicode.com/todos',
            { title, completed: false }
        );

        commit('newTodo', response.data);
    },
    async filterTodos({ commit }, e) {
      // Get selected number
      const limit = parseInt(
        e.target.options[e.target.options.selectedIndex].innerText
      );
  
      const response = await axios.get(
        `https://jsonplaceholder.typicode.com/todos?_limit=${limit}`
      );
  
      commit('setTodos', response.data);
    },
    async updateTodo({ commit }, updTodo) {
      const response = await axios.put(
        `https://jsonplaceholder.typicode.com/todos/${updTodo.id}`,
        updTodo
      );
  
      console.log(response.data);
  
      commit('updateTodo', response.data);
    }
};

const mutations = {
    setTodos: (state, todos) => (state.todos = todos),
    newTodo: (state, todo) => (state.todos.unshift(todo))
};

export default {
    state,
    getters,
    actions,
    mutations
};